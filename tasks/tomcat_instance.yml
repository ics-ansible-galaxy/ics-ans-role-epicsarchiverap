---
- name: "{{ instance.name }}: create tomcat instance base directories"
  file:
    path: "{{ epicsarchiverap_tomcats_base }}/{{ instance.name }}/{{ item.name }}"
    state: directory
    owner: "{{ item.owner }}"
    group: tomcat
    mode: 0750
  loop:
    - name: "conf/Catalina/localhost"
      owner: root
    - name: "webapps/{{ instance.name }}"
      owner: root
    - name: bin
      owner: tomcat
    - name: logs
      owner: tomcat
    - name: temp
      owner: tomcat
    - name: work
      owner: tomcat

- name: create tomcat setenv.sh script
  template:
    src: setenv.sh.j2
    dest: "{{ epicsarchiverap_tomcats_base }}/{{ instance.name }}/bin/setenv.sh"
    owner: root
    group: tomcat
    mode: 0644
  notify: "restart tomcat@{{ instance.name }}"

- name: create JMX exporter configuration
  template:
    src: jmx-exporter.yml.j2
    dest: "{{ epicsarchiverap_tomcats_base }}/{{ instance.name }}/conf/jmx-exporter.yml"
    owner: root
    group: tomcat
    mode: 0644
  notify: "restart tomcat@{{ instance.name }}"

- name: "{{ instance.name }}: create the instance xml files"
  template:
    src: "{{ item }}.j2"
    dest: "{{ epicsarchiverap_tomcats_base }}/{{ instance.name }}/conf/{{ item }}"
    owner: root
    group: tomcat
    mode: 0640
  vars:
    mysql_connection_pool: "{{ instance.mysql_connection_pool | default(false) }}"
    server_port: "{{ instance.server_port }}"
    connector_port: "{{ instance.connector_port }}"
    max_http_header_size: "{{ instance.max_http_header_size | default(false) }}"
  loop:
    - context.xml
    - server.xml
    - tomcat-users.xml
    - web.xml
  notify: "restart tomcat@{{ instance.name }}"

- name: "{{ instance.name }}: deploy the exploded web application"
  unarchive:
    src: "{{ epicsarchiverap_release_dir }}/{{ instance.name }}.war"
    dest: "{{ epicsarchiverap_tomcats_base }}/{{ instance.name }}/webapps/{{ instance.name }}"
    owner: root
    group: tomcat
    remote_src: true
    creates: "{{ epicsarchiverap_tomcats_base }}/{{ instance.name }}/webapps/{{ instance.name }}/WEB-INF/web.xml"
  notify: "restart tomcat@{{ instance.name }}"

- name: Remove unwanted buttons in mgmt application
  lineinfile:
    path: "{{ epicsarchiverap_tomcats_base }}/{{ instance.name }}/webapps/{{ instance.name }}/ui/index.html"
    regexp: "{{ item }}"
    state: absent
  loop:
    - <input type="button" id="archstatArchive"
    - <input type="button" id="archstatArchiveWithPeriod"
    - <input type="button" id="pause"
    - <input type="button" id="resume"
  when:
    - instance.name == "mgmt"
    - epicsarchiverap_mgmt_remove_buttons

- name: "{{ instance.name }}: start and enable the tomcat instance service"
  systemd:
    name: "tomcat@{{ instance.name }}"
    enabled: true
    state: started
